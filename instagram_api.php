<?php
/****************************************/
/*****       Instagram Count       *****/
/****************************************/
/*
function get_googleplus($user,$api_key) {
	// make sure the codes are readable
	$user = trim($user);
	$api_key = trim($api_key);
		
	// attempt to get cached request
	$transient_key = "_google_circ_counts";
	// If cached (transient) data are used, output an HTML
	// comment indicating such
	$cached = get_transient( $transient_key );
	if ( false !== $cached ) {
		return $cached;
	}
	// http get 
	$body = wp_remote_retrieve_body( wp_remote_get( 'https://www.googleapis.com/plus/v1/people/'.$user.'?key='.$api_key ) ); //.'?key='.$api_key ) );
	$json = json_decode($body, true);
	$plusones = $json['circledByCount'];
	
	set_transient( $transient_key, $plusones, 60*60*12 );

	return $plusones;
}
*/
function getInstagramFollowers($user){
	// Check for a transient with the count
	$count = get_transient('instagram_count2');
	if ($count !== false){
		return $count;
	}
	// transient not there? Lets go get the number
	$count = 0;

	$key = '1524145817.99ef8fd.ae1e31f0948c4de9bdf163bf9199d325'; // set for the app. This could expire, causing this to break
	// find the user id from the username
	$idsearch = 'https://api.instagram.com/v1/users/search?q='.$user.'&access_token='.$key;
	$getid = wp_remote_retrieve_body( wp_remote_get( $idsearch ) );
	$user = json_decode($getid, true);
	$user_id = $user['data'][0]['id'];
	
	// quit if this does not yield a numeric id
	if (!isset($user_id) || empty($user_id) || !is_numeric($user_id) ){
		return;
	}
	
	// Got the user id now get the dataset containing the followers
	$url = 'https://api.instagram.com/v1/users/'.$user_id.'/?access_token='.$key;
	$body = wp_remote_retrieve_body( wp_remote_get( $url ) ); // 
	$json = json_decode($body, true);

	$count = $json['data']['counts']['followed_by'];
	
	// now that we got the count, add it to the transient
	if (isset($count) && !empty($count) && is_numeric($count) ){
		set_transient('instagram_count2', $count, 60*60*48); // 72 hour cache
	}
	
	return $count;
}



?>