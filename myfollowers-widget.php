<?php
/*
Plugin Name: MyFollowerCount Widget
Plugin URI: http://www.castlemediagroup.com/
Description: Allows you to show the follower count of all your social media profiles
Version: 0.1
Author: Jim Camomile - Castle Media Group
Author URI:http://www.castlemediagroup.com/
License: GPL2
*/

// Load the various APIs in use
include_once('facebook_api.php');
include_once('twitter_api.php');
include_once('pinterest_api.php');
include_once('googleplus_api.php');
include_once('instagram_api.php');
include_once('linkedin_api.php');

if(!class_exists('MyFollowerCountWidget')) {
                  
	class MyFollowerCountWidget extends WP_Widget {
				
		function __construct() {
			$widget_ops = array('classname' => 'myfollowercount_widget', 'description' => __('Adds an aggregate count of all your social media followers.'));
			$control_ops = array('width' => 400, 'height' => 350);
			parent::__construct(false, 'MyFollowerCount Widget', $widget_ops, $control_ops);
		}

		function widget($args, $instance) {	
		
			/* Provide some defaults */
			$defaults = array( 'title' => 'My Followers', 'text_before_form' => '', 'text_after_form' => '');
			$instance = wp_parse_args( (array) $instance, $defaults );	
			// set the total count
			$total_count = 0;
			
			extract( $args );
			extract($instance);
			
			$title = apply_filters('widget_title', $title);
			
			// facebook
			
			// Get the Api count
			if (!empty($facebook_api)){
				$fb_api_count = facebook_followers($facebook_api);
			}
			// use the api count if it is available else use the static number
			$facebook_count = (!empty($fb_api_count) && is_numeric($fb_api_count) ? $fb_api_count : $facebook_count);
			$facebook_count = apply_filters('facebook_count', $facebook_count);
			$total_count += $facebook_count;
			
			$facebook_link = apply_filters('facebook_link', $facebook_link);

			// twitter
			// Get the Api count for those with a function to do so
			if (!empty($twitter_api)){
				$twit_api_count = twitter_followers($twitter_api);
			}
			
			// use the api count if it is available else use the static number
			$twitter_count = (!empty($twit_api_count) && is_numeric($twit_api_count) ? $twit_api_count : $twitter_count);
			$twitter_count = apply_filters('twitter_count', $twitter_count);
			$total_count += $twitter_count;
			
			// linkedin
			$linkedin_count = apply_filters('linkedin_count', $linkedin_count);
			$total_count += $linkedin_count;
			
			// Pinterest
			if (!empty($pinterest_api)){
				$pin_api_count = get_pinterest($pinterest_api);
			}
			
			$pinterest_count = (!empty($pin_api_count) && is_numeric($pin_api_count) && $pin_api_count > 0  ? $pin_api_count : $pinterest_count);
			
			$pinterest_count = apply_filters('pinterest_count', $pinterest_count);
			$total_count += $pinterest_count;
			
			// Google+
			if (!empty($gplus_user) && !empty($gplus_api)){
				//$gplus_api_count = get_googleplus($gplus_user,$gplus_api);				
			}
			
			if (!empty($gplus_user)){
				$gplus_api_count = getGplusFollowers($gplus_user);
				
			}
			
			
			$gplus_count = (!empty($gplus_api_count) && is_numeric($gplus_api_count) && $gplus_api_count > 0  ? $gplus_api_count : $gplus_count);
			//$gplus_count = apply_filters('gplus_count', $gplus_count);
			$total_count += $gplus_count;
			
			// Instagram
			if (!empty($instagram_user) && !empty($instagram_api)){
				//$instagram_api_count = get_instagram($instagram_user,$instagram_api);				
			}
			
			if (!empty($instagram_user)){
				$instagram_api_count = getInstagramFollowers($instagram_user);
				
			}
			
			
			$instagram_count = (!empty($instagram_api_count) && is_numeric($instagram_api_count) && $instagram_api_count > 0  ? $instagram_api_count : $instagram_count);
			//$instagram_count = apply_filters('instagram_count', $instagram_count);
			$total_count += $instagram_count;

			
			// RSS
			$rss_count = apply_filters('rss_count', $rss_count);
			$total_count += $rss_count;
			
			// email subscribers
			$email_count = apply_filters('email_count', $email_count);
			$total_count += $email_count;
			
			
			echo $before_widget;
			
			// insert the title
			 echo '<h3>'.$title.'</h3>'; 
			
			echo '<div class="count-wrapper">';
			
			if($facebook_count > 0){
				echo '<div class="count-box facebook">
						<a href="'.$facebook_link.'" target="_blank"><div class="img"></div></a>
						<span class="count">'.number_format($facebook_count,'0','',',').'</span>
						<span class="label">Fans</span>
						<a href="'.$facebook_link.'" class="btn" target="_blank">Like</a>
						<div class="fix"></div>
					</div>';
				}
			if($twitter_count > 0){
				echo '<div class="count-box twitter">
						<a href="'.$twitter_link.'" target="_blank"><div class="img"></div></a>
						<span class="count">'.number_format($twitter_count,'0','',',').'</span>
						<span class="label">Followers</span>
						<a href="'.$twitter_link.'" class="btn" target="_blank">Follow</a>
						<div class="fix"></div>
					</div>';
				}
			if($linkedin_count > 0){
				echo '<div class="count-box linkedin">
						<a href="'.$linkedin_link.'" target="_blank"><div class="img"></div></a>
						<span class="count">'.number_format($linkedin_count,'0','',',').'</span>
						<span class="label">Connections</span>
						<a href="'.$linkedin_link.'" class="btn" target="_blank">Connect</a>
						<div class="fix"></div>
					</div>';
			}
			if($pinterest_count > 0){
				echo '<div class="count-box pinterest">
						<a href="'.$pinterest_link.'" target="_blank"><div class="img"></div></a>
						<span class="count">'.number_format($pinterest_count,'0','',',').'</span>
						<span class="label">Subscribers</span>
						<a href="'.$pinterest_link.'" class="btn" target="_blank">Follow</a>
						<div class="fix"></div>
					</div>';
			}
			if($gplus_count > 0){
				echo '<div class="count-box gplus">
						<a href="'.$gplus_link.'" target="_blank"><div class="img"></div></a>
						<span class="count">'.number_format($gplus_count,'0','',',').'</span>
						<span class="label">Followers</span>
						<a href="'.$gplus_link.'" class="btn" target="_blank">Circle</a>
						<div class="fix"></div>
					</div>';
			}
			if($instagram_count > 0){
				echo '<div class="count-box instagram">
						<a href="'.$instagram_link.'" target="_blank"><div class="img"></div></a>
						<span class="count">'.number_format($instagram_count,'0','',',').'</span>
						<span class="label">Followers</span>
						<a href="'.$instagram_link.'" class="btn" target="_blank">Follow</a>
						<div class="fix"></div>
					</div>';
			}

			if($rss_count > 0){
				echo '<div class="count-box rss">
						<a href="'.$rss_link.'" target="_blank"><div class="img"></div></a>
						<span class="count">'.number_format($rss_count,'0','',',').'</span>
						<span class="label">Subscribers</span>
						<a href="'.$rss_link.'" class="btn" target="_blank">Subscribe</a>
						<div class="fix"></div>
					</div>';
			}
			if($email_count > 0){
				echo '<div class="count-box email">
						<a href="'.$email_link.'" target="_blank"><div class="img"></div></a>
						<span class="count">'.number_format($email_count,'0','',',').'</span>
						<span class="label">Subscribers</span>
						<a href="'.$email_link.'" class="btn" target="_blank">Subscribe</a>
						<div class="fix"></div>
					</div>';
			}
		
			echo '</div>';
			echo '<div class="total-wrapper">';			 
			echo '<div class="count-box">
					<span class="label">Total Platform </span> 
					<span class="number">'.number_format($total_count,'0','',',').'</span>';
			echo '</div></div>';
			// end after text
						
			echo $after_widget; 
		}

		function update($new_instance, $old_instance) {
			$instance = array();
			$instance['title'] = strip_tags( $new_instance['title'] );
			$instance['facebook_count'] =  str_replace(',','',$new_instance['facebook_count']);
			$instance['facebook_api'] =  $new_instance['facebook_api'];
			$instance['facebook_link'] =  $new_instance['facebook_link'];
			$instance['twitter_count'] =  str_replace(',','',$new_instance['twitter_count']);
			$instance['twitter_api'] =  $new_instance['twitter_api'];
			$instance['twitter_link'] =  $new_instance['twitter_link'];
			$instance['linkedin_count'] =  str_replace(',','',$new_instance['linkedin_count']);
			$instance['linkedin_link'] =  $new_instance['linkedin_link'];
			$instance['pinterest_api'] =  $new_instance['pinterest_api'];
			$instance['pinterest_count'] =  str_replace(',','',$new_instance['pinterest_count']);
			$instance['pinterest_link'] =  $new_instance['pinterest_link'];
			$instance['gplus_user'] =  $new_instance['gplus_user'];
			$instance['gplus_api'] =  $new_instance['gplus_api'];
			$instance['gplus_count'] =  str_replace(',','',$new_instance['gplus_count']);
			$instance['gplus_link'] =  $new_instance['gplus_link'];
			$instance['instagram_user'] =  $new_instance['instagram_user'];
			$instance['instagram_api'] =  $new_instance['instagram_api'];
			$instance['instagram_count'] =  str_replace(',','',$new_instance['instagram_count']);
			$instance['instagram_link'] =  $new_instance['instagram_link'];
			$instance['rss_count'] =  str_replace(',','',$new_instance['rss_count']);
			$instance['rss_link'] =  $new_instance['rss_link'];
			$instance['email_count'] =  str_replace(',','',$new_instance['email_count']);
			$instance['email_link'] =  $new_instance['email_link'];

			return $instance;			
		}

		function form($instance) {	
			global $woo_options;
			$defaults = array( 'title' => 'Our Followers', 'text_before_form' => '', 'text_after_form' => '');
			$instance = wp_parse_args( (array) $instance, $defaults );		
			
			extract($instance);
			$title = strip_tags($title);
			$siteurl = get_option('siteurl');
			$help_url = $siteurl . '/wp-content/plugins/' . basename(dirname(__FILE__)) . '/myfollowercount-help.html';

			?>

		


  			<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title (optional): </label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr( $title ); ?>"   id="<?php echo $this->get_field_id('title'); ?>" />
			</p>
			
  <div class="box facebook">
  			<div class="label">Facebook</div>
			 <p>
			<label for="<?php echo $this->get_field_id('facebook_api'); ?>">Facebook Page ID (optional): </label>
			<input type="text" name="<?php echo $this->get_field_name('facebook_api'); ?>" value="<?php echo esc_attr( $facebook_api ); ?>"   id="<?php echo $this->get_field_id('facebook_api'); ?>" />
			<div class="note"> This allows you to get the count from facebook dynamically</div>
			</p>

			 <p>
			<label for="<?php echo $this->get_field_id('facebook_count'); ?>">Facebook Fans: </label>
			<input type="text" name="<?php echo $this->get_field_name('facebook_count'); ?>" value="<?php echo esc_attr( $facebook_count ); ?>"   id="<?php echo $this->get_field_id('facebook_count'); ?>" />
			<div class="note">  If the facebook Page ID is not set above, then this number will be used</div>
			</p>
			
			 <p>
			<label for="<?php echo $this->get_field_id('facebook_link'); ?>">Facebook Page URL: </label>
			<input type="text" name="<?php echo $this->get_field_name('facebook_link'); ?>" value="<?php echo esc_attr( $facebook_link ); ?>"   id="<?php echo $this->get_field_id('facebook_link'); ?>" />
			</p>
			
</div>
<div class="box twitter">
  			<div class="label">Twitter</div>
			 <p>
			<label for="<?php echo $this->get_field_id('twitter_api'); ?>">Twitter ID (optional): </label>
			<input type="text" name="<?php echo $this->get_field_name('twitter_api'); ?>" value="<?php echo esc_attr( $twitter_api ); ?>"   id="<?php echo $this->get_field_id('twitter_api'); ?>" />
			<div class="note"> This allows you to get the count from twitter dynamically. To Find your Twitter ID, <a href="http://mytwitterid.com/" target="_blank">Visit My Twitter ID</a> and enter in your Twitter User. Courtesy of <a href="http://twittercounter.com/" target="_blank">Twitter Counter</a>  </div>
			</p>

			 <p>
			<label for="<?php echo $this->get_field_id('twitter_count'); ?>">Twitter Followers: </label>
			<input type="text" name="<?php echo $this->get_field_name('twitter_count'); ?>" value="<?php echo esc_attr( $twitter_count ); ?>"   id="<?php echo $this->get_field_id('twitter_count'); ?>" />
			<div class="note">If the Twitter user name is not set above, then this number will be used</div>
			</p>
	
			 <p>
			<label for="<?php echo $this->get_field_id('twitter_link'); ?>">Twitter URL: </label>
			<input type="text" name="<?php echo $this->get_field_name('twitter_link'); ?>" value="<?php echo esc_attr( $twitter_link ); ?>"   id="<?php echo $this->get_field_id('twitter_link'); ?>" />
			
			</p>
			
</div>
<div class="box linkedin">
  			<div class="label">LinkedIn</div>
			
			 <p>
			<label for="<?php echo $this->get_field_id('linkedin_count'); ?>">Linkedin Connections: </label>
			<input type="text" name="<?php echo $this->get_field_name('linkedin_count'); ?>" value="<?php echo esc_attr( $linkedin_count ); ?>"   id="<?php echo $this->get_field_id('linkedin_count'); ?>" />
			</p>
			
			 <p>
			<label for="<?php echo $this->get_field_id('linkedin_link'); ?>">Linkedin URL: </label>
			<input type="text" name="<?php echo $this->get_field_name('linkedin_link'); ?>" value="<?php echo esc_attr( $linkedin_link ); ?>"   id="<?php echo $this->get_field_id('linkedin_link'); ?>" />
			</p>
			
</div>
<div class="box pinterest">			
  			<div class="label">Pinterest</div>
			 <p>
			<label for="<?php echo $this->get_field_id('pinterest_api'); ?>">Website URL (for Pinterest) (optional): </label>
			<input type="text" name="<?php echo $this->get_field_name('pinterest_api'); ?>" value="<?php echo esc_attr( $pinterest_api ); ?>"   id="<?php echo $this->get_field_id('pinterest_api'); ?>" />
			<div class="note"> Adding your URL (i.e. http://www.authormedia.com/ ) here allows you to get the count from pinterest dynamically. Make sure it starts with "http://" and ends with a "/" </div>
			</p>

			 <p>
			<label for="<?php echo $this->get_field_id('pinterest_count'); ?>">Pinterest Followers: </label>
			<input type="text" name="<?php echo $this->get_field_name('pinterest_count'); ?>" value="<?php echo esc_attr( $pinterest_count ); ?>"   id="<?php echo $this->get_field_id('pinterest_count'); ?>" />
			</p>

			 <p>
			<label for="<?php echo $this->get_field_id('pinterest_link'); ?>">Pinterest URL: </label>
			<input type="text" name="<?php echo $this->get_field_name('pinterest_link'); ?>" value="<?php echo esc_attr( $pinterest_link ); ?>"   id="<?php echo $this->get_field_id('pinterest_link'); ?>" />
			</p>

</div>
<div class="box gplus">
  			<div class="label">Google Plus</div>
			<p>
			<label for="<?php echo $this->get_field_id('gplus_user'); ?>">Google Plus user ID (optional): </label>
			<input type="text" name="<?php echo $this->get_field_name('gplus_user'); ?>" value="<?php echo esc_attr( $gplus_user ); ?>"   id="<?php echo $this->get_field_id('gplus_user'); ?>" />
			<div class="note"> Adding your gplus user id and API code will allow you to dynamically show your number of followers. For Help finding your User ID, <a href="<?php echo $help_url; ?>" target="_blank">click here</a></div>
			</p>
			<p>
			<label for="<?php echo $this->get_field_id('gplus_api'); ?>">Google Plus API Code (optional): </label>
			<input type="text" name="<?php echo $this->get_field_name('gplus_api'); ?>" value="<?php echo esc_attr( $gplus_api ); ?>"   id="<?php echo $this->get_field_id('gplus_api'); ?>" />
			<div class="note"> Adding your gplus user id and API code will allow you to dynamically show your number of followers. For Help finding your API Code, <a href="<?php echo $help_url; ?>" target="_blank">click here</a></div>
			</p>
			 <p>
			<label for="<?php echo $this->get_field_id('gplus_count'); ?>">Google+ Followers: </label>
			<input type="text" name="<?php echo $this->get_field_name('gplus_count'); ?>" value="<?php echo esc_attr( $gplus_count ); ?>"   id="<?php echo $this->get_field_id('gplus_count'); ?>" />
			</p>
			
		 <p>
			<label for="<?php echo $this->get_field_id('gplus_link'); ?>">Google+ URL: </label>
			<input type="text" name="<?php echo $this->get_field_name('gplus_link'); ?>" value="<?php echo esc_attr( $gplus_link ); ?>"   id="<?php echo $this->get_field_id('gplus_link'); ?>" />
			</p>

</div>

<div class="box instagram">
  			<div class="label">Instagram</div>
			<p>
			<label for="<?php echo $this->get_field_id('instagram_user'); ?>">Instagram user name (optional): </label>
			<input type="text" name="<?php echo $this->get_field_name('instagram_user'); ?>" value="<?php echo esc_attr( $instagram_user ); ?>"   id="<?php echo $this->get_field_id('instagram_user'); ?>" />
			<div class="note"> Adding your Instagram user name will allow you to dynamically show your number of followers. </div>
			</p>
			 <p>
			<label for="<?php echo $this->get_field_id('instagram_count'); ?>">Instagram Followers: </label>
			<input type="text" name="<?php echo $this->get_field_name('instagram_count'); ?>" value="<?php echo esc_attr( $instagram_count ); ?>"   id="<?php echo $this->get_field_id('instagram_count'); ?>" />
			</p>
			
		 <p>
			<label for="<?php echo $this->get_field_id('instagram_link'); ?>">Instagram URL: </label>
			<input type="text" name="<?php echo $this->get_field_name('instagram_link'); ?>" value="<?php echo esc_attr( $instagram_link ); ?>"   id="<?php echo $this->get_field_id('instagram_link'); ?>" />
			</p>

</div>


<div class="box rss">			
  			<div class="label">RSS (Feedburner)</div>

			 <p>
			<label for="<?php echo $this->get_field_id('rss_count'); ?>">RSS Subscribers: </label>
			<input type="text" name="<?php echo $this->get_field_name('rss_count'); ?>" value="<?php echo esc_attr( $rss_count ); ?>"   id="<?php echo $this->get_field_id('rss_count'); ?>" />
			</p>

			 <p>
			<label for="<?php echo $this->get_field_id('rss_link'); ?>">Feedburner (RSS) URL: </label>
			<input type="text" name="<?php echo $this->get_field_name('rss_link'); ?>" value="<?php echo esc_attr( $rss_link ); ?>"   id="<?php echo $this->get_field_id('rss_link'); ?>" />
			</p>

</div>
<div class="box email">
  			<div class="label">Email Engine (Mailchimp)</div>

			 <p>
			<label for="<?php echo $this->get_field_id('email_count'); ?>">Email Subscribers: </label>
			<input type="text" name="<?php echo $this->get_field_name('email_count'); ?>" value="<?php echo esc_attr( $email_count ); ?>"   id="<?php echo $this->get_field_id('email_count'); ?>" />
			</p>      
			
			 <p>
			<label for="<?php echo $this->get_field_id('email_link'); ?>">Mailchimp URL: </label>
			<input type="text" name="<?php echo $this->get_field_name('email_link'); ?>" value="<?php echo esc_attr( $email_link ); ?>"   id="<?php echo $this->get_field_id('email_link'); ?>" />
			</p>       
			      
</div>

		<?php 
		}
	}
}

// Widget CSS
if ( ! function_exists( 'myfollowers_css_load' ) ) {
	function myfollowers_css_load() {
		$siteurl = get_option('siteurl');
		$url = $siteurl . '/wp-content/plugins/' . basename(dirname(__FILE__)) . '/mfc-styles.css';
		wp_register_style( 'mfc-styles', $url);
		wp_enqueue_style('mfc-styles');
	}
}
add_action( 'wp_head', 'myfollowers_css_load', 20 );

// CSS for Admin
if ( ! function_exists( 'myfollowers_admin_css_load' ) ) {
	function myfollowers_admin_css_load() {
		$siteurl = get_option('siteurl');
		$url = $siteurl . '/wp-content/plugins/' . basename(dirname(__FILE__)) . '/mfc_admin.css';
		wp_register_style( 'mfc-admin', $url);
		wp_enqueue_style( 'mfc-admin');
	}
}
add_action( 'admin_head', 'myfollowers_admin_css_load', 20 );





add_action( 'widgets_init', create_function( '', 'return register_widget("MyFollowerCountWidget");' ), 1 ); 
?>