<?php
function get_pinterest($url) {
	// attempt to get cached request
	$transient_key = "_pinterest_followers";
	// If cached (transient) data are used, output an HTML
	// comment indicating such
	$cached = get_transient( $transient_key );
	if ( false !== $cached ) {
		return $cached;
	}

	// make sure the the url ends in a /
	if (substr($url,-1) != '/'){$url = $url.'/';}
	$body = wp_remote_retrieve_body( wp_remote_get( 'http://api.pinterest.com/v1/urls/count.json?url='.$url ) );
	$json_string = preg_replace('/^receiveCount\((.*)\)$/', "\\1", $body);
	$json = json_decode($json_string, true);
	$pinheads = $json['count'];
	
	settype($pinheads, "integer");
	set_transient( $transient_key, $pinheads, 60*60*12 );

	return $pinheads;
}

?>