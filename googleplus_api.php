<?php
/****************************************/
/*****       GooglePlus Count       *****/
/****************************************/
function get_googleplus($user,$api_key) {
	// make sure the codes are readable
	$user = trim($user);
	$api_key = trim($api_key);
		
	// attempt to get cached request
	$transient_key = "_google_circ_counts";
	// If cached (transient) data are used, output an HTML
	// comment indicating such
	$cached = get_transient( $transient_key );
	if ( false !== $cached ) {
		return $cached;
	}
	// http get 
	$body = wp_remote_retrieve_body( wp_remote_get( 'https://www.googleapis.com/plus/v1/people/'.$user.'?key='.$api_key ) ); //.'?key='.$api_key ) );
	$json = json_decode($body, true);
	$plusones = $json['circledByCount'];
	
	set_transient( $transient_key, $plusones, 60*60*12 );

	return $plusones;
}

function getGplusFollowers($user){
	$url = 'https://plus.google.com/u/0/+'.$user.'/posts';
	$count = get_transient('gplus_count');
	if ($count !== false){
		//return $count;
	}
	$count = 0;
	$data = file_get_contents($url);
	//print_r('<pre style="padding: 10px; border: 1px solid #000; margin: 10px">'); print_r( $data ); print_r('</pre>'); 
	if (is_wp_error($data)) {
		return '!';
	} else {
		if (preg_match("#([0-9,\.]*)</span>(\s+)?follower#Uis", $data, $matches)) {
			$results =  preg_replace("/[,\.]/", '', $matches[1]);
		}
		// make sure it is not empty and is a number
		if ( isset($results) && !empty($results) && ctype_digit($results) ){
			$count = $results;
		}
	}
	//print_r('<pre style="padding: 10px; border: 1px solid #000; margin: 10px">'); print_r( $data ); print_r('</pre>'); 
	set_transient('gplus_count', $count, 60*60*48); // 72 hour cache
	return $count;
}

//$plusones = get_googleplus('113762408761633801599','AIzaSyAQxVWJEDD_0pqgHCHnemXiskYVANXRwFk');
//print_r('<pre>'); print_r($plusones); print_r('</pre>');
?>