<?php
/****************************************/
/*****       Twitter Count          *****/
/****************************************/
		/**
		* Simple twitter feed
		* Uses the Twitter Counter API available at http://twittercounter.com/pages/api/
		*/

		function twitter_followers($user = ''){
			
			//cache request
			$transient_key = "_twitter_followers";
			 
			// If cached (transient) data are used, output an HTML
			// comment indicating such
			$cached = get_transient( $transient_key );
			 
			if ( false !== $cached ) {
				return $cached;
			}
			// AuthorMedia API key obtained through - http://twittercounter.com/pages/api-signup/
			$user = trim($user);
			$apikey='1c4852705f597585a1bf23e44245658f';
			$apiurl = 'http://api.twittercounter.com/?twitter_id='.$user.'&apikey='.$apikey.'';
			// Request the API data, using the constructed URL
			$remote = wp_remote_get( $apiurl );
			 			 
			// If the API returns a valid response, the data will be
			// json-encoded; so decode it.
			$data = json_decode( $remote['body'] );
			$output = $data->followers_current;
			//print_r('<pre style="padding: 10px; border: 1px solid #000; margin: 10px word-wrap: normal">'); print_r($output ); print_r('</pre>');
			set_transient( $transient_key, $output, 60*60*12 );
			
			return $output;
		}

?>