<?php
/****************************************/
/*****       Facebook Count         *****/
/****************************************/
		/**
		* Simple Facebook feed
		* @return string of formatted API data
		*/

function facebook_followers($page_id){
	$api_url = 	'http://api.facebook.com/restserver.php?method=facebook.fql.query&query=SELECT%20fan_count%20FROM%20page%20WHERE%20page_id=';
	//cache request
	$transient_key = "_facebook_followers";
	 
	// If cached (transient) data are used, output an HTML
	// comment indicating such
	$cached = get_transient( $transient_key );
	 
	if ( false !== $cached ) {
		//return $cached;
	}

	$xml = @simplexml_load_file($api_url.$page_id."") or die ("a lot");
	$fans = $xml->page->fan_count;
	settype($fans, "integer");
	set_transient( $transient_key, $fans, 60*60*12 );
	return $fans;	
}

//alternate method using open graph

function facebook_og_followers($pagename){
//	$api_url = 	'http://api.facebook.com/restserver.php?method=facebook.fql.query&query=SELECT%20fan_count%20FROM%20page%20WHERE%20page_id=';
	$api_url = 	'http://graph.facebook.com/AuthorMedia';
	//cache request
	$transient_key = "_facebook_followers";
	 
	// If cached (transient) data are used, output an HTML
	// comment indicating such
	$cached = get_transient( $transient_key );
	 
	if ( false !== $cached ) {
		//return $cached;
	}

	$xml = @simplexml_load_file($api_url.$page_id."") or die ("a lot");
	$fans = $xml->page->fan_count;
	settype($fans, "integer");
	set_transient( $transient_key, $fans, 60*60*12 );
	return $fans;	
}

?>