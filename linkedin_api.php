<?php
/****************************************/
/*****       GooglePlus Count       *****/
/****************************************/
function get_linkedin($user_id) {
	// make sure the codes are readable
	$user_id = trim($user_id);

	// attempt to get cached request
	$transient_key = "_linkedin_cvountgs";
	// If cached (transient) data are used, output an HTML
	// comment indicating such
	$cached = get_transient( $transient_key );
	if ( false !== $cached ) {
		return $cached;
	}
	// http get 
	$api_url = "http://api.linkedin.com/v1/people/~";
	$oauth->fetch($api_url, null, OAUTH_HTTP_METHOD_GET, array('x-li-format' => 'json'));
	print_response($oauth);
	$json = json_decode($body, true);
	$plusones = $json['circledByCount'];
	
	//set_transient( $transient_key, $plusones, 60*60*12 );

	return $plusones;
}
//$user_id = '46939828';
//$linkys = get_linkedin($user_id);
//print_r('<pre>'); print_r($linkys); print_r('</pre>');
?>